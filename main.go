package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
)

var logLineRegexpArry = []string{
	`^(?P<BucketOwner>[^\s]+)`,
	`(?P<Bucket>[^\s]+)`,
	`(?P<Time>\[\d+\/\w{3}/\d{4}:\d{2}:\d{2}:\d{2}\s\+\d{4}\])`,
	`(?P<RemoteIP>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})`,
	`(?P<Requester>[^\s]+)`,
	`(?P<RequestID>[^\s]+)`,
	`(?P<Operation>[^\s]+)`,
	`(?P<Key>[^\s]+)`,
	`"(?P<RequestURI>[^"]+)"`,
	`(?P<HTTPStatus>\d{3})`,
	`(?P<ErrorCode>[^\s]+)`,
	`(?P<BytesSent>[^\s]+)`,
	`(?P<ObjectSize>[^\s]+)`,
	`(?P<TotalTime>[^\s]+)`,
	`(?P<TurnAround>[^\s]+)`,
	`"(?P<Referer>[^"]+)"`,
	`"(?P<UserAgent>[^"]+)"`,
	`(?P<VersionId>[^\s]+)`,
	`(?P<HostId>[^\s]+)`,
	`(?P<SignatureVersion>[^\s]+)`,
	`(?P<CipherSuite>[^\s]+)`,
	`(?P<AuthenticationType>[^\s]+)`,
	`(?P<HostHeader>[^\s]+)`,
	`(?P<TLSversion>[^\s]+)$`,
}

var (
	logDir         string
	output         string
	showHeaderOnly bool
	logLineRegexp  = regexp.MustCompile(strings.Join(logLineRegexpArry, `\s`))
)

type S3AccessLogLine struct {
	mapResult          map[string]string
	BucketOwner        string
	Bucket             string
	Time               string
	RemoteIP           string
	Requester          string
	RequestID          string
	Operation          string
	Key                string
	RequestURI         string
	HTTPStatus         string
	ErrorCode          string
	BytesSent          string
	ObjectSize         string
	TotalTime          string
	TurnAround         string
	Referer            string
	UserAgent          string
	VersionId          string
	HostId             string
	SignatureVersion   string
	CipherSuite        string
	AuthenticationType string
	HostHeader         string
	TLSversion         string
}

func (s *S3AccessLogLine) CSVString() string {
	var r []string
	for _, h := range s.Headers() {
		r = append(r, `"`+s.mapResult[h]+`"`)
	}
	return strings.Join(r, ",")
}

func (s *S3AccessLogLine) Headers() []string {
	return []string{
		"BucketOwner",
		"Bucket",
		"Time",
		"RemoteIP",
		"Requester",
		"RequestID",
		"Operation",
		"Key",
		"RequestURI",
		"HTTPStatus",
		"ErrorCode",
		"BytesSent",
		"ObjectSize",
		"TotalTime",
		"TurnAround",
		"Referer",
		"UserAgent",
		"VersionId",
		"HostId",
		"SignatureVersion",
		"CipherSuite",
		"AuthenticationType",
		"HostHeader",
		"TLSversion",
	}
}

func init() {
	flag.StringVar(&logDir, "logdir", "", "Specify the log directory path")
	flag.StringVar(&output, "output", "stdout", "Specify the output,default stdout")
	flag.BoolVar(&showHeaderOnly, "header-only", false, "show header only")
	flag.Parse()
}

func main() {
	if showHeaderOnly {
		fmt.Println(strings.Join((&S3AccessLogLine{}).Headers(), ","))
		return
	}

	if logDir == "" {
		logrus.Fatalf("log directory can't empty")
	}

	_ = filepath.Walk(logDir, func(path string, info os.FileInfo, err error) error {
		if info.IsDir() {
			return err
		}

		f, err := os.OpenFile(path, os.O_RDONLY, os.ModePerm)
		if err != nil {
			logrus.Errorf("open file error: %v", err)
			return err
		}
		defer f.Close()

		rd := bufio.NewReader(f)
		for {
			line, err := rd.ReadString('\n')
			if err != nil {
				if err == io.EOF {
					break
				}
				logrus.Errorf("read file line error: %v", err)
				return err
			}
			sll := parseLine(line)
			_ = sll
			fmt.Println(sll.CSVString())
		}

		return err
	})
}

func parseLine(line string) *S3AccessLogLine {
	match := logLineRegexp.FindStringSubmatch(strings.TrimSpace(line))
	result := make(map[string]string)
	for i, name := range logLineRegexp.SubexpNames() {
		if i != 0 && name != "" {
			result[name] = match[i]
		}
	}
	result["Time"] = func(_ts string) string {
		t, err := time.Parse("[02/Jan/2006:15:04:05 -0700]", _ts)
		if err != nil {
			return ""
		}
		return t.Format("2006-01-02 15:04:05 -0700")
	}(result["Time"])

	return &S3AccessLogLine{
		mapResult:          result,
		BucketOwner:        result["BucketOwner"],
		Bucket:             result["Bucket"],
		Time:               result["Time"],
		RemoteIP:           result["RemoteIP"],
		Requester:          result["Requester"],
		RequestID:          result["RequestID"],
		Operation:          result["Operation"],
		Key:                result["Key"],
		RequestURI:         result["RequestURI"],
		HTTPStatus:         result["HTTPStatus"],
		ErrorCode:          result["ErrorCode"],
		BytesSent:          result["BytesSent"],
		ObjectSize:         result["ObjectSize"],
		TotalTime:          result["TotalTime"],
		TurnAround:         result["TotalTime"],
		Referer:            result["Referer"],
		UserAgent:          result["UserAgent"],
		VersionId:          result["VersionId"],
		HostId:             result["HostId"],
		SignatureVersion:   result["HostId"],
		CipherSuite:        result["CipherSuite"],
		AuthenticationType: result["AuthenticationType"],
		HostHeader:         result["HostHeader"],
		TLSversion:         result["TLSversion"],
	}
}
